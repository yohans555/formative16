package com.nexsoft.app.repo;

import com.nexsoft.app.model.Shipadress;

import org.springframework.data.repository.CrudRepository;

public interface Shipadressrepository extends CrudRepository<Shipadress, Integer> {
}


