package com.nexsoft.app.service;

import java.util.ArrayList;
import java.util.Optional;

import javax.transaction.Transactional;

import com.nexsoft.app.model.Shipadress;
import com.nexsoft.app.repo.Shipadressrepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Shipmentservice {
    @Autowired
     private Shipadressrepository shipment;

    @Transactional
    public ArrayList<Shipadress> get(){
        return (ArrayList<Shipadress>) shipment.findAll();
    }

    @Transactional
    public Optional<Shipadress> getbyId(int id){
        return shipment.findById(id);
    }   


    @Transactional
	public void save(Shipadress product) {
		shipment.save(product);
	}

    @Transactional
    public void delete(int id){
        shipment.deleteById(id);
    }

    
}
