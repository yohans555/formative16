package com.nexsoft.app.Controller;


import java.util.Optional;

import com.nexsoft.app.model.Shipadress;
import com.nexsoft.app.service.Shipmentservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;


@RestController
public class productcontroller {
    @Autowired
    Shipmentservice shipmentservice;

    @GetMapping("/home")
    public ModelAndView home(){
        ModelAndView mv = new ModelAndView("listproduk2");
        mv.addObject("list", shipmentservice.get());
        return mv;
    }

    @RequestMapping(value ="update/Save/{id}", method = RequestMethod.POST)
    public void add(@ModelAttribute("shipadress") Shipadress shipadress, @PathVariable("id") int id){
        shipadress.setId(id);
        shipmentservice.save(shipadress);
    }

    @RequestMapping("/delete/{id}")
	public ModelAndView delete(@PathVariable("id") int id) {
		ModelAndView mv = new ModelAndView("listproduk2");
        shipmentservice.delete(id);;
		mv.addObject("list", shipmentservice.get());
		return mv;
	}

    @RequestMapping("/detail/{id}")
	public ModelAndView detail(@PathVariable("id") int id) {
		ModelAndView mv = new ModelAndView("detailproduk");
        Optional<Shipadress>list = shipmentservice.getbyId(id);
        Shipadress shipadress = list.get();
        mv.addObject("list", shipadress);
		return mv;
	}

    @RequestMapping("/update/{id}")
	public ModelAndView update(@PathVariable("id") int id) {
		ModelAndView mv = new ModelAndView("addsender");
        Optional<Shipadress>list = shipmentservice.getbyId(id);
        Shipadress shipadress = list.get();
        mv.addObject("shipadress", shipadress);
		return mv;
	}
    
}
