<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Sending Address</h1>
	<form:form action = "Save/${shipadress.getId()}" modelAttribute="shipadress">
		Enter Recepient: <form:input path="recepient"/><br/>
		gender
		<form:radiobutton path="gender" value="male"/>male
		<form:radiobutton path="gender" value="female"/>female
		<br>
		Enter Street: <form:input path="street"/><br/>
		Enter zipcode: <form:input path="zipcode"/><br/>
		Enter city: <form:input path="city"/><br/>
		Enter dis: <form:input path="district"/><br/>
		Enter province: <form:input path="province"/><br/>
		country: 
		<form:select path="country">
			<form:option value="indonesia">indonesia</form:option>
			<form:option value="malaysia">malaysia</form:option>
			<form:option value="singapore">singapore</form:option>
		</form:select><br/>
		Assurance (10%): <form:checkbox path="assurance" value="true"/>  <br>
		<form:textarea path = "comment" rows = "5" cols = "30" /> <br>
		Enter price: <form:input path="price"/><br/>
		Enter deliveryprice: <form:input path="deliveryprice"/><br/>
		<button type = "submit">Save</button>
	</form:form>
</body>
</html>