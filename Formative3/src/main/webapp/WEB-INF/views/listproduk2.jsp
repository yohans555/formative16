<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>List of Shipment</h1>
	<table border = "1">
		<tr>
			<th>Recepient</th>
			<th>Gender</th>
			<th>Street</th>
			<th>Province</th>
			<th>Country</th>
			<th>Assurance</th>
			<th>Price</th>
			<th>action</th>
		</tr>
		<c:forEach items="${list}" var="e">
			<tr>		
				<td>${e.getRecepient()}</td>
				<td>${e.getGender()}</td>
				<td>${e.getStreet()}</td>
				<td>${e.getProvince()}</td>
				<td>${e.getCountry()}</td>
				<td>${e.isAssurance()}</td>
				<td>${e.getPrice()}</td>
				<td>
					<a onclick="delFunction()" id="func" href = "${pageContext.request.contextPath}/delete/${e.getId()}">Delete</a> <br>
					<a onclick="delFunction()" id="func" href = "${pageContext.request.contextPath}/update/${e.getId()}">Update</a> <br>
					<a onclick="delFunction()" id="func" href = "${pageContext.request.contextPath}/detail/${e.getId()}">Detail</a> <br>
				</td>
			</tr>
		</c:forEach>
	</table>

    <script>
        function delFunction() {
            confirm("are you sure?");
        }
    </script>
</body>
</html>