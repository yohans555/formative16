<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
    <!-- CSS -->
    <meta name="robots" content="noindex,follow" />

</head>
<body>
    <table border="1">
      <tr>
        <th>ID</th>
        <th>Recepient</th>
        <th>Gender</th>
        <th>Adress</th>
        <th>Assurance</th>
        <th>Comment</th>
        <th>Price</th>
        <th>delivery cost</th>
      </tr>
      <tr>
        <td>${list.getId()}</td>
        <td>${list.getRecepient()}</td>
        <td>${list.getGender()}</td>
        <td>${list.getStreet()}, ${list.getCity()},${list.getProvince()},${list.getCountry()},${list.getZipcode()}</td>
        <td>${list.isAssurance()}</td>
        <td>${list.getComment()}</td>
        <td>${list.getPrice()}</td>
        <td>${list.getDeliveryprice()}</td>
      </tr>
    </table>
</body>
</html>