package com.nexsoft.app.model;

public class Shipadress {
    String recepient, gender, street,zipcode,district,city,province,country,comment;
    boolean assurance;
    int price,deliveryprice;
    int id;

    public Shipadress(){};
    public Shipadress(String recepient, String gender, String street, String zipcode, String district, String city,
            String province, String country, String comment, boolean assurance, int price, int deliveryprice, int id) {
        this.recepient = recepient;
        this.gender = gender;
        this.street = street;
        this.zipcode = zipcode;
        this.district = district;
        this.city = city;
        this.province = province;
        this.country = country;
        this.comment = comment;
        this.assurance = assurance;
        this.price = price;
        this.deliveryprice = deliveryprice;
        this.id = id;
    }
    public String getRecepient() {
        return recepient;
    }
    public void setRecepient(String recepient) {
        this.recepient = recepient;
    }
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public String getStreet() {
        return street;
    }
    public void setStreet(String street) {
        this.street = street;
    }
    public String getZipcode() {
        return zipcode;
    }
    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }
    public String getDistrict() {
        return district;
    }
    public void setDistrict(String district) {
        this.district = district;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getProvince() {
        return province;
    }
    public void setProvince(String province) {
        this.province = province;
    }
    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }
    public String getComment() {
        return comment;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }
    public boolean isAssurance() {
        return assurance;
    }
    public void setAssurance(boolean assurance) {
        this.assurance = assurance;
    }
    public int getPrice() {
        return price;
    }
    public void setPrice(int price) {
        this.price = price;
    }
    public int getDeliveryprice() {
        return deliveryprice;
    }
    public void setDeliveryprice(int deliveryprice) {
        this.deliveryprice = deliveryprice;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    
    
}
