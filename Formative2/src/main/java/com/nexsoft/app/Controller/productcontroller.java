package com.nexsoft.app.Controller;

import java.sql.*;

import com.nexsoft.app.model.Shipadress;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;


@RestController
public class productcontroller {


    @RequestMapping(value ="/save", method = RequestMethod.POST)
    public void add(@ModelAttribute("shipadress") Shipadress shipadress) throws ClassNotFoundException, SQLException{
        // productservice.save(product);
        Class.forName("com.mysql.jdbc.Driver");
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/formative16", "root", "Semarang37!");
        String query = "insert into delivery" +
        "(recepient,gender,street,zipcode,district,city,country,comment,price,deliveryprice,assurance,province)"+
        "values(?,?,?,?,?,?,?,?,?,?,?,?)";

        PreparedStatement ps =con.prepareStatement(query);
        ps.setString(1, shipadress.getRecepient());
        ps.setString(2 , shipadress.getGender());
        ps.setString(3 , shipadress.getStreet());
        ps.setString(4 , shipadress.getZipcode());
        ps.setString(5 , shipadress.getDistrict());
        ps.setString(6 , shipadress.getCity());
        ps.setString(7, shipadress.getCountry());
        ps.setString(8, shipadress.getComment());
        ps.setInt(9, shipadress.getPrice());
        ps.setInt(10, shipadress.getDeliveryprice());
        ps.setBoolean(11, shipadress.isAssurance());
        ps.setString(12, shipadress.getProvince());
        ps.executeUpdate();
        ps.close();
    }

    @GetMapping(value="/addsender")
    public ModelAndView showProductForm() {
        ModelAndView mv = new ModelAndView("addsender");
        mv.addObject("shipadress", new Shipadress());
        return mv;
    }
    
}
